package poo;


public class Clase1 {

    /*la clase se crea sin constructor por ende se asume que esta el constructor por defecto que es igual a
            public Clase1(){
            }
        */

    //creacion de modificador de acceso default tanto en variables como en método
    protected int mivar = 5;
    protected int mivar2 = 7;

    protected String mimetodo(){
        return "El valor de mivar2 es: " + mivar2;

    }
}
