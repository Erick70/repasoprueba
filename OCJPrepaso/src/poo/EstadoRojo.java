package poo;

public class EstadoRojo extends EstadoSemaforo{
    public EstadoRojo( Semaforo objSemaforo ) {
        this.objSemaforo = objSemaforo;
    }
    // -------------------------------------------

    public void mostrar() {
        System.out.println("Alerta roja");
    }
}
